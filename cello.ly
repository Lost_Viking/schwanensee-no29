\language "deutsch"

rNumber = #(define-music-function
     (parser location num)
     (number?)
   #{  
     \mark #num 
     \set Score.markFormatter = #format-mark-box-numbers
   #})

addExtraSpaceAtBeginningOfMeasure = #(define-music-function (parser location x-extent) (pair?) 
#{ 
	\once \overrideProperty Score.PaperColumn.X-extent $x-extent
#})

 

\header{
  title = "No. 29"
  subtitle = "Scène Finale"
  composer = "Pyotr Ilyich Tchaikovsky"
  instrument = "Violoncello"
  copyright = "V1.1"
  tagline = ""
}

fpp = #(make-dynamic-script "fpp")



\relative c''{ 
  \override Score.StemTremolo.beam-width = 1.1
  \override Score.StemTremolo.beam-thickness = 0.4
  \override Score.StemTremolo.extra-offset = #'(0 . -0.3)
  \override Score.StemTremolo.slope = 0.5
  
  \compressFullBarRests
  \override Score.RehearsalMark.self-alignment-X = 0
  \override Score.RehearsalMark.Y-offset = 5 
  \override MultiMeasureRest.space-increment = #5
  \clef bass 
  \key e \major 
  \time 3/4  
  \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
  \once \override Score.RehearsalMark.extra-offset = #'(0 . -.5)  
  \rNumber #17
  \once \override Score.MetronomeMark.extra-offset = #'(3.5 . 2) 
  \tempo "Andante"
  
   h,2_"(Les prince entre en courant.)" cis8(dis)
   dis8[(gis,]) dis'[(e)] fis[(gis)]
   gis8[(h,)] cis[(e)] gis[ (\clef tenor h)] 
   his2.
   c2 a8[(h)]     
   h8[(dis,)] dis[(a')] a[(c,)]
   c8[(dis)]dis[(a)] a[(h)]
   \rNumber #18
   \bar "."
   \clef bass
   <gis e>2.:32 \ff
   <gis e>2.:32
   <gis e>2.:32
   <gis e>2.:32
   <a f>2.:32
   <a f>2.:32
   <a f>2.:32
   <gis e>2:32 e4:32
   e2.:32^"unisono"
   \set countPercentRepeats = ##t
   \set repeatCountVisibility = #(every-nth-repeat-count-visible 1)
   \repeat percent 6 {e2.:32} 
   e8 r8 r4 r4
   \repeat unfold 3 {e,8[e8]}
   e4 r4 r 4
   R2.^\fermataMarkup 
   %override Score.StemTremolo.extra-offset = #'(0 . +0.3)
  
  
   \bar "||"  
   \key c \major 
   \time 4/4
   \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
   \once \override Score.RehearsalMark.extra-offset = #'(0 . -2)  
   \rNumber #19 
   \once \override Score.MetronomeMark.extra-offset = #'(3.5 . .5) 
   \tempo "Allegro agitato"
   \once \override MultiMeasureRest.space-increment = #5
   r8 <a>4 \p <a>8 r8 <a>4 <a>8
   r8 <a>4 <a>8 r8 a4 a8
   r8 <a>4 <a>8 r8 <a>4 <a>8
   r8 <a>4 <a>8 r8 f4(e8)
   r8 a4 c4 d4 f8
   
   a8 a,4 a8 r8 a4 a8
   r8 <a>4 <a>8 r8 <a>4 <a>8
   r8 <a>4 <a>8 r8 f4(e8)
   
   r8 <a>4 <a>8 r8 <a'>4 \cresc <a>8 \!
   r8 <g>4 <g>8 r8 <f>4 <f>8
   r8 <e>4 <e>8 r8 <e'>4 <e>8
   r8 <d>4 <d>8 r8 <c>4 <c>8
   
   r8 <h>4 <h>8 r8 <e,>8 r8 <a>8
   r8 <g>4 <g>8 r8 <f>4 <f>8
   r8 <e>4 <e>8 r8 <e'>4 <e>8
   r8 <d>4 <d>8 r8 <c>4 <c>8  
   r8 b4 b8 r8 d4 d8
   r8 e4 e8 r8 d4 d8
   
   r8 b4 b8 r8 d4 d8
   r8 e4 e8 r8 d4 d8
   
   \rNumber #20 \bar "."
   \override Score.StemTremolo.extra-offset = #'(0 . +0.3)
   b8\f r8 r4 r8 a[:16[gis:16 e:16] 
   a8 r8 r4 r8 h[:16[gis:16 fis:16] 
   h8 h a gis r8 g fis e
   r8 e[d h] r8 e[d h]
   h' r8 r4 r8 h[:16[gis:16 fis:16] 
   h r8 r4 r8 cis[:16[his:16 gis:16]   
   cis8 c h ais r8 a gis fis
   r8 fis e cis r8 fis e cis 
   cis' r8 r4 r8 cis[:16[his:16 gis:16]
   cis r8 r4 r8 es[:16\cresc \![d:16 b:16]
   r8 e[:16[dis:16 h:16] r8 f'[:16[e:16 c:16]
   r8 fis[:16[eis:16 cis:16] r8 fis[:16[eis:16 cis:16]
   r8 fis[:16[eis:16 cis:16] r8 fis[:16[eis:16 cis:16]
   
  
   fis,,1:32 \ff
   fis1:32
   fis1:32
   fis1:32
   r4 g'2 g,4
   r4 cis'2 cis,4
   r4 g'2 g,4
   r4 cis'2 cis,4
  
  % g8:16 fis:16 eis:16 e:16 g8:16 fis:16 eis:16 e:16 
%     \override Score.RehearsalMark.extra-offset = #'(0 . .5) 
%     \rNumber #20
  \bar "."
   \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)     
   \rNumber #21
    fis8:16 eis:16 e:16 dis:16  \override Score.StemTremolo.extra-offset = #'(0 . -0.3) d:16 cis:16 h:16 ais:16 
    
   % fis'8:16 eis:16 e:16 dis:16 d:16 cis:16 h:16 ais:16 
    d:16 cis:16 h:16 ais:16 h:16 ais:16 a:16 ais:16 
     \override Score.StemTremolo.extra-offset = #'(0 . +0.3)  
    ais'8:16 a:16 gis:16 g:16 fis:16 e:16 d:16 cis:16 
    fis8:16 e:16 d:16 cis:16 \stemDown d:16 cis:16 his:16 cis:16
    R1 R1 \clef tenor 
    ais''8:16 a:16 gis:16 g:16 fis:16 e:16 d:16 cis:16
    fis8:16 e:16 d:16 cis:16 d:16 cis:16 his:16 cis:16


    \repeat tremolo 2 { g'8:8 fis:8}\repeat tremolo 2 { g8:8 fis:8}
    \repeat tremolo 2 { gis8:8 fis:8}\repeat tremolo 2 { gis8:8 fis:8}
    
    \repeat tremolo 2 { a8:8 fis:8}\repeat tremolo 2 { a8:8 fis:8}
    \repeat tremolo 2 { a8:8 fis:8}\repeat tremolo 2 { a8:8 fis:8}
    
    %\repeat tremolo 2 { a(fis)} \repeat tremolo 2 { a(fis)}
    %\repeat tremolo 2 { a(fis)} \repeat tremolo 2 { a(fis)}
    \bar "."
    \once \override Score.RehearsalMark.extra-offset = #'(.7 . -2)
    \clef bass
    \rNumber #22   
    \tupletSpan 2
     \tuplet 3/2 { 
       fis,4 \ff \breathe cis d e fis g
       r4 e fis g ais h
       r4 e, fis g ais h
       r4 ais h cis d e
     }  
     R1 *4
     
     
     
     
     \tuplet 3/2 { 
       g4 \mark "poco ritenuto" fis_"(Odette tombe das le bras du prince)" e d cis h
       ais g fis e d cis 
       h ais h cis d e        
     }
     eis-> \mark "ritenuto." fis-> gis-> ais-> R1    
     \bar "||"
     \key d \major 
     \time 2/2
     \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
     \once \override Score.RehearsalMark.extra-offset = #'(0 . -2)  
     \rNumber #23
     \once \override Score.MetronomeMark.extra-offset = #'(3.5 . .5) 
     \tempo "Moderato e maestoso."
     \tupletSpan 2
       \tuplet 3/2 {
         <d fis,>4 \ff <d fis,>4 <d fis,>4
            \repeat unfold 3{<d fis,>4}
            \repeat unfold 6{<e g,>4}
            h,4 cis d d e fis fis e d d cis h
            
            
            \repeat unfold 6{<d' fis,>4}
            \repeat unfold 6{<e g,>4}
            h,4 cis d d e fis g fis e e d cis
            
            \repeat unfold 6{<d' fis,>4}
            \repeat unfold 6{<e g,>4}
            h,4 cis d d e fis fis e d d cis h
            
            \repeat unfold 6{<d' fis,>4}
            \repeat unfold 6{<e g,>4}
             h,4 cis d d e fis g fis e d cis h
       }
       \clef tenor
       cis'2 d2 ^"largamente" \ff
       \rNumber #24
       e fis4 g
       a2. g4
       fis2 g4 a
       
       h2. a4 g2 a4 h4 
       cis2. h4 fis2 h,2
       cis2 d2 e2 fis4 g4
       a2. g4 fis2 g4 a4 
       h2. a4 g2 a4 h4
       \tupletSpan 1
       \tuplet 3/2 {
         c2 g c
         h1 h2
         ais2 fis ais
         h1 h2
         c2 g c
         h1 h2
         ais2 fis ais
         h1 h2
         \rNumber #25
         \bar "."
         c2 g c
         c2 g c
         g1.(g1) fis2
         c'2 g c
         c2 g c
         g1.(g1) fis2
         gis2 \ff fis gis
         gis2 fis gis
         a2 fis a 
         a2 fis a
         ais2 fis ais
         ais2 fis ais 
         ais2 e ais
         ais2 fis ais 
       }
       \clef bass
      \stemUp
       fis,,4:8 gis:8 ais:8 h:8          
          \stemDown
          cis4:8 h:8 cis:8 d:8          
          e4:8 \mark "ritenuto" d:8 e:8 fis:8      
          gis4:8 fis:8 gis:8 ais:8
       
       

        
        \bar "||"
        
      \key h \major 
       \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
       %\once \override Score.RehearsalMark.extra-offset = #'(0 . -2)  
       \rNumber #26
       \tempo "Meno Mosso."
               
        <h fis>2:8 \fff <h fis>2:8
        <gis e'>1:8
        <dis' fis,>1:8
        <dis fis,>1:8
        <dis fis,>2:8 <dis fis,>2:8
        <dis g,>2:8 <dis g,>2:8
        
        <dis fis,>1:8      
        <e gis,>1:8
        <h fis>2:8 <h fis>2:8
        <gis e'>1:8
        <dis' fis,>1:8
        <dis fis,>1:8
        <dis fis,>2:8 <dis fis,>2:8
        <e g,>2:8 <e g,>2:8
        <dis fis,>1:8
        gis,1:8
        \clef tenor
        
        dis'2:16 e:16 fis:16 gis4:16 ais:16
        h1:16 h2:16 h,:16 dis:16 e:16
        fis:16 gis4:16 ais:16
        
        \tuplet 3/2 {
         h2 g h
         a1 a2
         g2 e g
         fis1 fis2
         f2 d f
         e1 e2
         dis!2 h dis
         d1 d2
        }
        
        \tuplet 6/4 {
         \repeat unfold 12 {dis4}
         dis dis h h cis cis 
         dis dis e e fis fis 
        } 
         \clef bass
         cis,1(cis)        
         fis1(fis)
         
        
               
     
       \bar "||"
       %\once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
       \once \override Score.RehearsalMark.extra-offset = #'(0 . -2)  
       \rNumber #27      
        h,8_"(Apparition des cygenes au dessus du lac.)"  \pp \tempo "Moderatro."   
         \repeat unfold 7 {h8} 
         h'8 \cresc \! \repeat unfold 7 {h8}
         \repeat unfold 16 {gis8}        
         \repeat unfold 16 {dis'8}
         \repeat unfold 16 {cis8}
         h8 \fpp \repeat unfold 7 {h8} h8 \cresc \repeat unfold 7 {h8}
         \repeat unfold 8 {gis'8}                 
         \repeat unfold 8 {fis8} 
         \repeat unfold 16 {e8}     
         
         \rNumber #28
         <e>8 \fff \repeat unfold 15  {<e>8}
         
         
         \set countPercentRepeats = ##t
         \set repeatCountVisibility = #(every-nth-repeat-count-visible 1)
         \repeat percent 8 {  \repeat unfold 8 {<h>8} } \break
         h,1(h1) h1 h1 h1\fermata
         \bar "|."
%         
%     }
 
  %r1 mark (oh, pardonne moi, dit le prince etc. La dernère scène.)  
 
}

\layout {
    \context {
      \Score
      \override SpacingSpanner.shortest-duration-space = 2.05 %#(ly:make-moment 1/1)
    }
}
