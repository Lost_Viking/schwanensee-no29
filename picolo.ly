rNumber = #(define-music-function
     (parser location num)
     (number?)
   #{  
     \mark #num 
     \set Score.markFormatter = #format-mark-box-numbers
   #})

addExtraSpaceAtBeginningOfMeasure = #(define-music-function (parser location x-extent) (pair?) 
#{ 
	\once \overrideProperty Score.PaperColumn.X-extent $x-extent
#})

 

\header{
  title = "No. 29"
  subtitle = "Scène Finale"
  composer = "Pyotr Ilyich Tchaikovsky"
  tagline = ""
  instrument = "Piccolo"
  copyright = "V1.0"
}


\relative c'''{ 
  \compressFullBarRests
  \override Score.RehearsalMark.self-alignment-X = 0
  \override Score.RehearsalMark.Y-offset = 5 
  \override MultiMeasureRest.space-increment = #5
  \clef treble 
  \key e \major 
  \time 3/4  
  \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
  \once \override Score.RehearsalMark.extra-offset = #'(0 . -.5)  
  \rNumber #17
  \once \override Score.MetronomeMark.extra-offset = #'(3.5 . 2) 
  \tempo "Andante"
  R2. 
  R2._"(Les prince entre en courant.)"
  R2. 
  R2.* 4    
  \rNumber #18
  R2.*18
  R2.^\fermataMarkup 
  \bar "||"  
  \key c \major 
  \time 4/4
  \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
  \once \override Score.RehearsalMark.extra-offset = #'(0 . -2)  
  \rNumber #19 
  \once \override Score.MetronomeMark.extra-offset = #'(3.5 . .5) 
  \tempo "Allegro agitato"
  \once \override MultiMeasureRest.space-increment = #5
   R1*20  
   \override Score.RehearsalMark.extra-offset = #'(0 . .5) 
   \rNumber #20
   R1*17
   
   g8\ff(fis eis e) g8(fis eis e)
   g8(fis eis e) g8(fis eis e)
   g8(fis eis e) g8(fis eis e)
   g8(fis eis e) g8(fis eis e)
   \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
    
  \rNumber #21
   R1*6
   fis4\ff fis2 fis4 ~
   fis4 fis2 fis4
   \relative c''''{
     \repeat tremolo 2 { g8(fis)} \repeat tremolo 2 { g(fis)}
     \repeat tremolo 2 { gis(fis)} \repeat tremolo 2 { gis(fis)}
     \repeat tremolo 2 { a(fis)} \repeat tremolo 2 { a(fis)}
     \repeat tremolo 2 { a(fis)} \repeat tremolo 2 { a(fis)}
     ais4 
     \once \override Score.RehearsalMark.extra-offset = #'(.7 . -2)
     \rNumber #22 r4 r2    
   }
    R1 * 5
    \tupletSpan 2
    \tuplet 3/2 { 
      r4  cis d e fis g
      r4 fis g ais b cis
    }
    R1 \mark "poco ritenuto"
    R1 
    R1_"(Odette tombe das le bras du prince)"  
    R1 \mark "ritenuto."
    R1 
    \bar "||"
    \key d \major 
    \time 2/2
    \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
    \once \override Score.RehearsalMark.extra-offset = #'(0 . -2)  
    \rNumber #23
    \once \override Score.MetronomeMark.extra-offset = #'(3.5 . .5) 
    \tempo "Moderato e maestoso."
    \relative c''{
       fis1\ffff
       b,4 cis d e
       fis2. d4
       fis2. d4
       fis2. b,4
       d4 ais g d'
       b1 ~ 
       b4 cis b cis
      
       fis1 
       b,4 cis d e
       fis2. d4
       fis2. d4
       fis2. b,4
       d4 ais g d'
       b1 ~ 
       b2 r2
       
       \rNumber #24
        R1*4  
       
       \tupletSpan 2
       \tuplet 3/2 {
         e4(g b) b(g e)
         e(g b) b(g e)
         eis(gis cis) cis(gis eis) 
         fis(ais cis) 
       }
       r2       
       r1 r1 r1 r1 
      \tupletSpan 2
       \tuplet 3/2 {
         e,4(g b) b(g e)
         f(g b)  b(g f)
         e(g c) c(g e) 
         e(g b) b(g e)
       }
       R1
       \tupletSpan 2
       \tuplet 3/2 {
         e4(g b) b(g e)
         e(g c) c(g e) 
         e(g b)  b(g f)
         R1.
         e4(g b) b(g e)
       }
       
       \rNumber #25
       \tupletSpan 1
       \tuplet 3/2 {
        c'2 g c
        c g c
        g1.~
        g1 fis2
        
        c'2 g c
        c g c
        g1.~
        g1 fis2
        
        gis\ff fis gis
        gis fis fis
        a fis a
        a fis a
        ais fis ais
        ais fis ais
        ais e ais 
        ais fis ais                    
       }
       r1 r1^"ritenuto." r1 r1
       \bar "||"
       
     \key b \major 
      \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
      \once \override Score.RehearsalMark.extra-offset = #'(3 . 0)  
      \rNumber #26
      b2\fff( ais4 a4)
      gis( gis' fis e)
      dis1(dis)
      dis2 cis4( b)
      ais1
      b8 b(cis dis e fis gis ais, b1)
      b2\fff( ais4 a4)
      gis( gis' fis e)
      dis1(dis)
      dis2 cis4( b)
      ais1
      b8 b(cis dis e fis gis ais, b2) b,2
      dis2 e
      fis2 gis4 ais4 
      b1(b2) b,2 
      dis e
      fis2 gis4 ais4
      \tuplet 3/2{
        b2 g b
        ais1 ais2
        g2 e g
        fis1 fis2
        f2 d f
        e1 e2
        dis2 b dis
        d1 d2
      }
      dis1(dis 
      \tuplet 3/2{
        dis2) \breathe b cis
        dis2 e fis
      }
      g1(g)
      fis1(fis)
      \bar "||"
      %\once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
      \once \override Score.RehearsalMark.extra-offset = #'(0 . -2)  
      \rNumber #27
       b,2  \once \override Score.MetronomeMark.padding = #2 \tempo "Moderatro." r    
      \compressFullBarRests
      R1_"                     (Apparition des cygenes au dessus du lac.)"
      R1*14  
     % \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
      \once \override Score.RehearsalMark.extra-offset = #'(0 . -2)  
      R1
      \rNumber #28
      dis1 \ff
      R1
      dis1
      R1 dis dis dis
      R1*4
      R1^\fermataMarkup  
      \bar "|."
        
    }
 
  %r1 mark (oh, pardonne moi, dit le prince etc. La dernère scène.)  
 
}

\layout {
    \context {
      \Score
      \override SpacingSpanner.shortest-duration-space = 2.2 %#(ly:make-moment 1/1)
    }
}
