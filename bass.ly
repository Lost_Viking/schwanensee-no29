\language "deutsch"

rNumber = #(define-music-function
     (parser location num)
     (number?)
   #{  
     \mark #num 
     \set Score.markFormatter = #format-mark-box-numbers
   #})

addExtraSpaceAtBeginningOfMeasure = #(define-music-function (parser location x-extent) (pair?) 
#{ 
	\once \overrideProperty Score.PaperColumn.X-extent $x-extent
#})

fpp = #(make-dynamic-script "fpp")

 

\header{
  title = "No. 29"
  subtitle = "Scène Finale"
  composer = "Pyotr Ilyich Tchaikovsky"
  instrument = "Bassi"
  copyright = "V1.1"
}


\relative c{ 
  \override Score.StemTremolo.beam-width = 1.1
  \override Score.StemTremolo.beam-thickness = 0.4
  \override Score.StemTremolo.extra-offset = #'(0 . -0.3)
  \override Score.StemTremolo.slope = 0.5
  
  \compressFullBarRests
  \override Score.RehearsalMark.self-alignment-X = 0
  \override Score.RehearsalMark.Y-offset = 5 
  \override MultiMeasureRest.space-increment = #5
  \clef bass 
  \key e \major 
  \time 3/4  
  \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
  \once \override Score.RehearsalMark.extra-offset = #'(0 . -.5)  
  \rNumber #17
  \once \override Score.MetronomeMark.extra-offset = #'(3.5 . 2) 
  \tempo "Andante"
  
   e4_"pizz." r4 e4 
   \repeat unfold 6 {e4 r4 e4}

   \rNumber #18
   \bar "."
   <e>2.:32^"arco" \ff
   <<
      \set countPercentRepeats = ##t
      \set repeatCountVisibility = #(every-nth-repeat-count-visible 1)
      \repeat percent 13 {<e>2.:32} 
      {
        s2.
        \repeat unfold 5 {s2.}
        s2.\dim  s2. s2. s2. 
        s2. s2. \mf
        s2. <e>2.:32 \pp
      }
    >> 
   \tupletSpan 4
   \tuplet 3/2{
      \repeat unfold 9 {e8}
    }
    
   e8 r8 e8_"pizz." r8 e8 r8  
   e8 r8 r4 r4
   R2.^\fermataMarkup 
   %override Score.StemTremolo.extra-offset = #'(0 . +0.3)
  
  
   \bar "||"  
   \key c \major 
   \time 4/4
   \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
   \once \override Score.RehearsalMark.extra-offset = #'(0 . -2)  
   \rNumber #19 
   \once \override Score.MetronomeMark.extra-offset = #'(3.5 . .5) 
   \tempo "Allegro agitato"
   \once \override MultiMeasureRest.space-increment = #5
   r8 <a,>4_"arco" \p <a>8 r8 <a>4 <a>8
   r8 <a>4 <a>8 r8 a4 a8
   r8 <a>4 <a>8 r8 <a>4 <a>8
   r8 <a>4 <a>8 r8 f4(e8)
   r8 a4 c4 d4 f8
   
   a8 a,4 a8 r8 a4 a8
   r8 <a>4 <a>8 r8 <a>4 <a>8
   r8 <a>4 <a>8 r8 f4(e8)
   
   r8 <a>4 <a>8 r8 <a'>4 \cresc <a>8 \!
   r8 <g>4 <g>8 r8 <f>4 <f>8
   r8 <e>4 <e>8 r8 <e'>4 <e>8
   r8 <d>4 <d>8 r8 <c>4 <c>8
   
   r8 <b>4 <b>8 r8 <e,>8 r8 <a>8
   r8 <g>4 <g>8 r8 <f>4 <f>8
   r8 <e>4 <e>8 r8 <e'>4 <e>8
   r8 <d>4 <d>8 r8 <c>4 <c>8  
   r8 b4 b8 r8 d4 d8
   r8 e4 e8 r8 d4 d8
   
   r8 b4 b8 r8 d4 d8
   r8 e4 e8 r8 d4 d8
   
   \rNumber #20 \bar "."
   b8\f r8 r4 r8 a[gis e] 
   a8 r8 r4 r8 h[ais fis] 
   h8 h a gis r8 g fis e
   r8 e[d h] r8 e[d h]
   h' r8 r4 r8 h[gis fis] 
   h r8 r4 r8 cis[his gis]   
   cis8 cis h ais r8 a gis fis
   r8 fis e cis r8 fis e cis 
   cis' r8 r4 r8 cis[his gis]
   cis r8 r4 r8 es\cresc \![d b]
   r8 e[dis h] r8 f'[e c]
   r8 fis[eis cis] r8 fis[eis cis]
   r8 fis[eis cis] r8 fis[eis cis]
   
   fis,,1:32 \ff
   fis1:32
   fis1:32
   fis1:32
   r4 g'2 g,4
   r4 cis'2 cis,4
   r4 g'2 g,4
   r4 cis'2 cis,4
  
  % g8:16 fis:16 eis:16 e:16 g8:16 fis:16 eis:16 e:16 
%     \override Score.RehearsalMark.extra-offset = #'(0 . .5) 
%     \rNumber #20
  \bar "."
   \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)     
   \rNumber #21
    ais'8 a gis g fis e d cis fis e d cis d c his c
    R1 *10
    
    \bar "."
    \once \override Score.RehearsalMark.extra-offset = #'(.7 . -2)
    \clef bass
    \rNumber #22   
    \tupletSpan 2
     \tuplet 3/2 { 
       fis4 \ff ais h cis d e
       r4 cis d e fis g 
     }  
     R1 R1 R1*4
     
     \tuplet 3/2 { 
       g4 \mark "poco ritenuto" fis_"(Odette tombe das le bras du prince)" e d cis h
       ais g fis e d cis 
       h ais h cis d e        
     }
     eis-> \mark "ritenuto." fis-> gis-> ais-> R1    
     \bar "||"
     \key d \major 
     \time 2/2
     \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
     \once \override Score.RehearsalMark.extra-offset = #'(0 . -2)  
     \rNumber #23
     \once \override Score.MetronomeMark.extra-offset = #'(3.5 . .5) 
     \tempo "Moderato e maestoso."
         h,1 h1
         \tupletSpan 2
         \tuplet 3/2 {
           h4 cis d d e fis fis e d d cis h
         } 
         
         h1 h1
         \tupletSpan 2
         \tuplet 3/2 {
           h4 cis d d e fis g fis e e d cis
         }
         
         h1 h1
         \tupletSpan 2
         \tuplet 3/2 {
           h4 cis d d e fis fis e d d cis h
         }
         
         h1 h1
         \tupletSpan 2
         \tuplet 3/2 {
           h4 cis d d e fis g fis e d cis h
         }         
       
       \rNumber #24
       a2^"largamente" \ff r2
       g'2 r2
       fis1(fis4) r4 r2
       
       e2 r2 d2 r2 cis2 r2 fis h a r2
       g2 r2
       fis1(fis4) r4 r2
       e2 r2 d2 r2 c1 e fis e c e fis e
       
      \rNumber #25
      \bar "."
      c(c)
       
       
       \tupletSpan 1
       \tuplet 3/2 {
         cis2 fis, cis'
         cis2 fis, cis'
       }
       c1(c)
       
       \tuplet 3/2 {
         cis2 fis, cis'
         cis2 fis, cis'
       }
       d1\ff
       d' c, c' ais, ais' 
       g fis 

       fis,4 gis ais h          
       cis4 h cis d          
       e4 \mark "ritenuto" d e fis      
       gis4 fis gis ais
       
       

        
        \bar "||"
        
      \key h \major 
       \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
       %\once \override Score.RehearsalMark.extra-offset = #'(0 . -2)  
       \rNumber #26
       \tempo "Meno Mosso."
       \tuplet 3/2{
         h,4\fff h4 h4
       }
       h2
       
       <<
       \repeat percent 14 {
         
         \tuplet 3/2{
           h4 h4 h4
         }
         h2
       }
       {
         s1*14
         
       }
        >>
        gis'1 fis1 e1 dis1
        gis1 fis e d dis e fis gis ais
        h eis, fis(fis) fis fis cis(cis) fis(fis)
     
       \bar "||"
       %\once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
       \once \override Score.RehearsalMark.extra-offset = #'(0 . -2)  
       \rNumber #27      
        h,2_"(Apparition des cygenes au dessus du lac.)"  \p \tempo "Moderatro."    
        r2
        R1 * 16
        \rNumber #28
        h1 \fff R1 h1 R1 h1
        h1 h1 h1(h1) h1 h1 h1\fermata
       
       \bar "|."
%         
%     }
 
  %r1 mark (oh, pardonne moi, dit le prince etc. La dernère scène.)  
 
}

\layout {
    \context {
      \Score
      \override SpacingSpanner.shortest-duration-space = 2.0 %#(ly:make-moment 1/1)
    }
}
