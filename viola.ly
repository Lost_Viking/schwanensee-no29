\language "deutsch"

rNumber = #(define-music-function
     (parser location num)
     (number?)
   #{  
     \mark #num 
     \set Score.markFormatter = #format-mark-box-numbers
   #})

addExtraSpaceAtBeginningOfMeasure = #(define-music-function (parser location x-extent) (pair?) 
#{ 
	\once \overrideProperty Score.PaperColumn.X-extent $x-extent
#})

 

\header{
  title = "No. 29"
  subtitle = "Scène Finale"
  composer = "Pyotr Ilyich Tchaikovsky"
  instrument = "Viola"
  copyright = "V1.0"
}


fpp = #(make-dynamic-script "fpp")


\relative c''{ 
  \override Score.StemTremolo.beam-width = 1.1
  \override Score.StemTremolo.beam-thickness = 0.4
  \override Score.StemTremolo.extra-offset = #'(0 . -0.3)
  \override Score.StemTremolo.slope = 0.5
  
  \compressFullBarRests
  \override Score.RehearsalMark.self-alignment-X = 0
  \override Score.RehearsalMark.Y-offset = 5 
  \override MultiMeasureRest.space-increment = #5
  \clef alto 
  \key e \major 
  \time 3/4  
  \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
  \once \override Score.RehearsalMark.extra-offset = #'(0 . -.5)  
  \rNumber #17
  \once \override Score.MetronomeMark.extra-offset = #'(3.5 . 2) 
  \tempo "Andante"
  
   h,2_"(Les prince entre en courant.)" cis8(dis)
   dis8[(gis,]) dis'[(e)] fis[(gis)]
   gis8[(h,)] cis[(e)] gis[(h)] 
   his2.
   c2 a8[(h)]     
   h8[(dis,)] dis[(ais')] ais[(c,)]
   c8[(dis)]dis[(a)] a[(h)]
   \rNumber #18
   \bar "."
   <gis' e>2.:32 \ff
   <gis e>2.:32
   <gis e>2.:32
   <gis e>2.:32
   <a f>2.:32
   <a f>2.:32
   <a f>2.:32
   <gis e>2.:32\dim
   <a  fis>2.:32 
   <gis e>2:32  
  \override Score.StemTremolo.extra-offset = #'(0 . 0)
   e4:32
   fis2.:32
   e2.:32
   fis:32\mf
   e4:32 h:32 gis:32
   fis2.:32\p
   e2.:32 
   e2.:32 
   e4 r4 r4
   R2.^\fermataMarkup 
   
   \bar "||"  
   \key c \major 
   \time 4/4
   \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
   \once \override Score.RehearsalMark.extra-offset = #'(0 . -2)  
   \rNumber #19 
   \once \override Score.MetronomeMark.extra-offset = #'(3.5 . .5) 
   \tempo "Allegro agitato"
   \once \override MultiMeasureRest.space-increment = #5
   r8 <a e>4 \p  <a e>8 r8 <a e>4 <a e>8
   r8 <a e>4 <a e>8 r8 f4 f8
   r8 <a e>4 <a e>8 r8 <a e>4 <a e>8
   r8 <a e>4 <a e>8 r8 gis4 <d' gis,>8
   r8 a4 a4 a4 a8
   
   c8 <a e>4 <a e>8 r8 f4 f8
   r8 <a e>4 <a e>8 r8 <a e>4 <a e>8
   r8 <a e>4 <a e>8 r8 gis4 <d' gis,>8
   r8 <a e>4 <a e>8 r8 c4 \cresc c8 \!
   
   r8 d4 d8 r8 d4 d8
   r8 e4 e8 r8 e4 e8
   r8 f4 f8 r8 f4 f8
   r8 fis4 fis8 r8 e8 r8 c8
   
   r8 d4 d8 r8 d4 d8
   r8 e4 e8 r8 e4 e8   
   r8 f4 f8 r8 f4 f8
   r8 f4 f8 r8 f4 f8
   
   r8 e4 e8 r8 f4 f8
   r8 f4 f8 r8 f4 f8
   r8 e4 e8 r8 f4 f8
   %r8 b4 b8 r8 a4 a8
   %r8 gis4 gis8 r8 a4 a8
   \rNumber #20 \bar "."
   f8\f b,[:16[d:16 f:16] f4(e)
   r8 c8:16[e:16 a:16] g4(fis)
   fis2 h,8:16[cis:16 d:16 e:16]
   fis4.(d8) fis4.(d8)
   r8 d:16[ fis:16 h:16] g4(fis)
   
   r8 d8:16[fis:16 h:16] a4(gis)
   gis2 cis,8:16 dis:16 e:16 fis:16
   gis4.(e8) gis4.(e8)
   r8 e8:16 gis:16 cis:16 a4.(gis8)
   r8 e8:16 \cresc gis:16 cis:16 h4.(ais8)
   c4.(h8) des4.(c8) 
   d4.(cis8) d4.(cis8)
   d4.(cis8) d4.(cis8)
   
   fis,8:16 \ff eis:16 e:16 dis:16 e:16 dis:16 d:16 cis:16
   d8:16   cis:16 g':16 fis:16 g:16 fis:16 g:16 fis:16 
   fis8:16 eis:16 e:16 dis:16 e:16 dis:16 d:16 cis:16
   d8:16   cis:16 g':16 fis:16 g:16 fis:16 g:16 fis:16 

   g8:16 fis:16 eis:16 e:16 g8:16 fis:16 eis:16 e:16 
   g8:16 fis:16 eis:16 e:16 g8:16 fis:16 eis:16 e:16 
   g8:16 fis:16 eis:16 e:16 g8:16 fis:16 eis:16 e:16
   g8:16 fis:16 eis:16 e:16 g8:16 fis:16 eis:16 e:16
  % g8:16 fis:16 eis:16 e:16 g8:16 fis:16 eis:16 e:16 
%     \override Score.RehearsalMark.extra-offset = #'(0 . .5) 
%     \rNumber #20
  \bar "."
   \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)     
   \rNumber #21
    R1*2
    fis8:16 eis:16 e:16 dis:16 d:16 cis:16 h:16 ais:16 
    d:16 cis:16 h:16 ais:16 h:16 ais:16 a:16 ais:16 
    ais'8:16 a:16 gis:16 g:16 fis:16 e:16 d:16 cis:16 
    fis8:16 e:16 d:16 cis:16 d:16 cis:16 his:16 cis:16
    fis'8:16 f:16 e:16 dis:16 d:16 cis:16 h:16 ais:16
    d:16 cis:16 h:16 ais:16 h:16 ais:16 a:16 ais:16

    \repeat tremolo 2 { g8:8 fis:8}\repeat tremolo 2 { g8:8 fis:8}
    \repeat tremolo 2 { gis8:8 fis:8}\repeat tremolo 2 { gis8:8 fis:8}
    
    \repeat tremolo 2 { a8:8 fis:8}\repeat tremolo 2 { a8:8 fis:8}
    \repeat tremolo 2 { a8:8 fis:8}\repeat tremolo 2 { a8:8 fis:8}
    
    %\repeat tremolo 2 { a(fis)} \repeat tremolo 2 { a(fis)}
    %\repeat tremolo 2 { a(fis)} \repeat tremolo 2 { a(fis)}
    \bar "."
    \once \override Score.RehearsalMark.extra-offset = #'(.7 . 0)
    \rNumber #22   
    \tupletSpan 2
     \tuplet 3/2 { 
       fis4 \ff \breathe fis, g ais h cis
       r4 ais h cis dis eis

       r4 g, ais h cis d
       r4 cis d e fis gis
 
       r4 ais, h cis d e
       r4 cis d e eis g
       
       r4 e fis g ais h
       r4 ais h cis d e
        
     }        
     
     R1 \mark "poco ritenuto"
     R1_"                  (Odette tombe das le bras du prince)" 
     R1  
     R1 \mark "ritenuto"
     R1 
     \bar "||"
     \key d \major 
     \time 2/2
     \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
     \once \override Score.RehearsalMark.extra-offset = #'(0 . -2)  
     \rNumber #23
     \once \override Score.MetronomeMark.extra-offset = #'(3.5 . .5) 
     \tempo "Moderato e maestoso."
     \tupletSpan 2
       \tuplet 3/2 {
            <fis, h,>4 \ffff <fis h,>4 <fis h,>4
            \repeat unfold 3{<fis h,>4}
            \repeat unfold 6{<g h,>4}
       }
       <d h>2. \tuplet 3/2{ <d h>8[<d h> <d h>]}
       <d h>2. \tuplet 3/2{ <d h>8[<d h> <d h>]}
       \tuplet 3/2 {
            \repeat unfold 6{<fis h,>4}
            \repeat unfold 6{<g ais,>4}
       }
        <fis h,>2. \tuplet 3/2{ <h d,>8[<h d,> <h d,>]}
        <h d,>2 <g h,>2
        \tuplet 3/2 {
            \repeat unfold 6{<fis h,>4}
            \repeat unfold 6{<g h,>4}
       }
       <d h>2. \tuplet 3/2{ <d h>8[<d h> <d h>]}
       <d h>2. \tuplet 3/2{ <d h>8[<d h> <d h>]}
       \tuplet 3/2 {
            \repeat unfold 6{<fis h,>4}
            \repeat unfold 6{<g ais,>4}
       }
        <fis h,>2. \tuplet 3/2{ <h d,>8[<h d,> <h d,>]}
   

       <e, h>2 <d h>^"largamente" \ff
       \rNumber #24
       cis2 d
       e fis4 g
       a2. g4
       fis2 g4 a
       
       h2. a4 g2 a4 h4 
       cis2. h4 fis2 h,2
       cis2 d2 e2 fis4 g4
       a2. g4 fis2 g4 a4 
       h2. a4 g2 a4 h4
       \tupletSpan 1
       \tuplet 3/2 {
         c2 g c
         h1 h2
         ais2 fis ais
         h1 h2
         c2 g c
         h1 h2
         ais2 fis ais
         h1 h2
         \rNumber #25
         \bar "."
         c2 g c
         c2 g c
         g1.(g1) fis2
         c'2 g c
         c2 g c
         g1.(g1) fis2
         gis2 \ff fis gis
         gis2 fis gis
         a2 fis a 
         a2 fis a
         ais2 fis ais
         ais2 fis ais 
         ais2 e ais
         ais2 fis ais 
       }
       

          fis,4:8 gis:8 ais:8 h:8          
          cis4:8 h:8 cis:8 d:8          
          e4:8 \mark "Ritenuto" d:8 e:8 fis:8      
          gis4:8 fis:8 gis:8 ais:8
       
       

        
        \bar "||"
        
      \key h \major 
       \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
       %\once \override Score.RehearsalMark.extra-offset = #'(0 . -2)  
       \rNumber #26
       \tempo "Meno Mosso."
               
        <h dis,>2:8 \fff <fis dis>2:8
        <gis h,>1:8
        <h fis>1:8
        <h fis>1:8
        <h fis>2:8 <h fis>2:8
        <g h,>2:8 <g h,>2:8
        <fis h,>1:8
        <gis h,>1:8
        
        <h dis,>2:8 <fis dis>2:8
        <gis h,>1:8
        <h fis>1:8
        <h fis>1:8
        <h fis>2:8 <h fis>2:8
        <g h,>2:8 <g h,>2:8
        <fis h,>1:8
        <e h>2:8 h2:8
        
        dis2:16 e:16 fis:16 gis4:16 ais:16
        h1:16 h2:16 h,:16 dis:16 e:16
        fis:16 gis4:16 ais:16
        
        \tuplet 3/2 {
         h2 g h
         a1 a2
         g2 e g
         fis1 fis2
         f2 d f
         e1 e2
         dis2 h dis
         d1 d2
        }
        
        \tuplet 6/4 {
         \repeat unfold 12 {dis4}
         dis dis h h cis cis 
         dis dis e e fis fis 
         <e' g,>1.:8 <e g,>1.:8
         
         <cis fis,>1.:8
         <cis fis,>1.:8
        }
               
     
       \bar "||"
       %\once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
       \once \override Score.RehearsalMark.extra-offset = #'(0 . -2)  
       \rNumber #27      
        fis,,8_"(Apparition des cygenes au dessus du lac.)"  \pp \tempo "Moderatro."   
         \repeat unfold 7 {fis8} 
         dis'8 \cresc \! \repeat unfold 7 {dis8}
         \repeat unfold 16 {dis8}
         \repeat unfold 16 {fis8}
         \repeat unfold 16 {e8}
         
         dis8 \fpp \repeat unfold 7 {dis8}         
         dis8 \cresc \repeat unfold 7 {dis8}
         \repeat unfold 8 {h'8}
         \repeat unfold 8 {ais8}
         \repeat unfold 16 {gis8}         
         <cis gis>8\ff \repeat unfold 15  {<cis gis>8}
         
         <fis, dis>8 \repeat unfold 7  {<fis dis>8}
         
         
         \set countPercentRepeats = ##t
         \set repeatCountVisibility = #(every-nth-repeat-count-visible 1)
         \rNumber #28
         \repeat percent 6 {  \repeat unfold 8 {<fis dis>8} } \break
         h,1(h1) h1 h1 h1\fermata
         \bar "|."
%         
%     }
 
  %r1 mark (oh, pardonne moi, dit le prince etc. La dernère scène.)  
 
}

\layout {
    \context {
      \Score
      \override SpacingSpanner.shortest-duration-space = 2.1 %#(ly:make-moment 1/1)
    }
}
