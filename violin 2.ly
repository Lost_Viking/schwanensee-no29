\language "deutsch"

rNumber = #(define-music-function
     (parser location num)
     (number?)
   #{  
     \mark #num 
     \set Score.markFormatter = #format-mark-box-numbers
   #})

addExtraSpaceAtBeginningOfMeasure = #(define-music-function (parser location x-extent) (pair?) 
#{ 
	\once \overrideProperty Score.PaperColumn.X-extent $x-extent
#})

 

\header{
  title = "No. 29"
  subtitle = "Scène Finale"
  composer = "Pyotr Ilyich Tchaikovsky"
  instrument = "Violin II"
  %tagline = "d1.1"
  copyright = "V1.1"
}

fpp = #(make-dynamic-script "fpp")


\relative c'''{ 
  \override Score.StemTremolo.beam-width = 1.1
  \override Score.StemTremolo.beam-thickness = 0.4
  \override Score.StemTremolo.extra-offset = #'(0 . -0.3)
  \override Score.StemTremolo.slope = 0.5
  
  \compressFullBarRests
  \override Score.RehearsalMark.self-alignment-X = 0
  \override Score.RehearsalMark.Y-offset = 5 
  \override MultiMeasureRest.space-increment = #5
  \clef treble 
  \key e \major 
  \time 3/4  
  \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
  \once \override Score.RehearsalMark.extra-offset = #'(0 . -.5)  
  \rNumber #17
  \once \override Score.MetronomeMark.extra-offset = #'(3.5 . 2) 
  \tempo "Andante"
  
   h,2_"(Les prince entre en courant.)" cis8(dis)
   dis8(gis,) dis'(e) fis(gis)
   gis8(h,) cis(e) gis(h) 
   his2.
   c2 a8(h)     
   h8(dis,) dis(a') a(c,)
   c8(dis)dis(a) a(h)
   \rNumber #18
   \bar "."
   <gis' h,>2.:32 \ff
   <gis h,>2.:32
   <gis h,>2.:32
   <gis his,>2.:32
   <f a,>2.:32
   <f a,>2.:32
   <f a,>2.:32
   <gis h, >2.:32\dim 
   < a h,>2.:32
   <gis h, >2:32  
  \override Score.StemTremolo.extra-offset = #'(0 . +0.3)
   gis,4:32
   a2.:32
   gis2.:32
   a2.:32\mf
   gis4:32 e:32 h:32
   a2.:32 \p
   gis2.:32 
   gis2.:32 
   gis4 r4 r4
   R2.^\fermataMarkup 
   
   \bar "||"  
   \key c \major 
   \time 4/4
   \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
   \once \override Score.RehearsalMark.extra-offset = #'(0 . -2)  
   \rNumber #19 
   \once \override Score.MetronomeMark.extra-offset = #'(3.5 . .5) 
   \tempo "Allegro agitato"
   \once \override MultiMeasureRest.space-increment = #5
   r8 c4 \p c8 r8 c4 c8
   r8 c4 c8 r8 a4 a8
   r8 c4 c8 r8 c4 c8
   r8 c4 c8 r8 d4 e8
   r8 c4 e4 f4 d8
   
   e8 c4 c8 r8 a4 a8
   r8 c4 c8 r8 c4 c8
   r8 c4 c8 r8 d4 e8
   r8 c4 c8 r8 e4 \cresc e8 \!
   
   r8 g4 g8 r8 g4 g8
   r8 g4 g8 r8 g4 g8
   r8 a4 a8 r8 a4 a8
   r8 h4 h8 r8 gis8 r8 e8
   
   r8 g4 g8 r8 g4 g8
   r8 g4 g8 r8 g4 g8   
   r8 a4 a8 r8 a4 a8
   
   r8 b4 b8 r8 a4 a8   
   r8 gis4 gis8 r8 a4 a8
  
   r8 b4 b8 r8 a4 a8   
   r8 gis4 gis8 r8 a4 a8

   \override Score.StemTremolo.extra-offset = #'(0 . -0.3)  
   \rNumber #20 \bar "."
   b8\f d,[:16[f:16 b:16] f4(e)
   r8 e8:16[a:16 c:16] g4(fis)
   fis2 h,8:16[cis:16 d:16 e:16]
   fis4.(d8) fis4.(d8)
   r8 fis8:16[ h:16 d:16] g,4(fis)
   
   r8 fis8:16[h:16 d:16] a4(gis)
   gis2 cis,8:16 dis:16 e:16 fis:16
   gis4.(e8) gis4.(e8)
   r8 gis8:16 cis:16 e:16 a,4.(gis8)
   r8 gis8:16 \cresc cis:16 e:16 h4.(ais8)
   c4.(h8) des4.(c8) 
   d4.(cis8) d4.(cis8)
   d4.(cis8) d4.(cis8)
   
   \override Score.StemTremolo.extra-offset = #'(0 . +0.3)  
   fis8:16 \ff eis:16 e:16 dis:16 e:16 dis:16 d:16 cis:16
   \override Score.StemTremolo.extra-offset = #'(0 . -0.3)  
   d8:16   cis:16 g:16 fis:16 g:16 fis:16 g:16 fis:16 
   \override Score.StemTremolo.extra-offset = #'(0 . 0.3)  
   fis'8:16 eis:16 e:16 dis:16 e:16 dis:16 d:16 cis:16
   \override Score.StemTremolo.extra-offset = #'(0 . -0.3)  
   d8:16   cis:16 g:16 fis:16 g:16 fis:16 g:16 fis:16 

  
   \override Score.StemTremolo.extra-offset = #'(0 . +0.3)  
   g'8:16 fis:16 eis:16 e:16 g8:16 fis:16 eis:16 e:16 
   g8:16 fis:16 eis:16 e:16 g8:16 fis:16 eis:16 e:16 
   g8:16 fis:16 eis:16 e:16 g8:16 fis:16 eis:16 e:16 
   g8:16 fis:16 eis:16 e:16 g8:16 fis:16 eis:16 e:16 
%     \override Score.RehearsalMark.extra-offset = #'(0 . .5) 
%     \rNumber #20
  \bar "."
   \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)     
   \rNumber #21
    R1*4
    fis8:16 eis:16 e:16 dis:16 d:16 cis:16 h:16 ais:16 
    d:16 cis:16 h:16 ais:16   \stemDown h:16 ais:16 a:16 ais:16 
    ais'8:16 a:16 gis:16 g:16 fis:16 e:16 d:16 cis:16 
    fis8:16 e:16 d:16 cis:16 d:16 cis:16 his:16 cis:16
    
    \repeat tremolo 2 { g'8:8 fis:8}\repeat tremolo 2 { g8:8 fis:8}
    \repeat tremolo 2 { gis8:8 fis:8}\repeat tremolo 2 { gis8:8 fis:8}
    \repeat tremolo 2 { a8:8 fis:8}\repeat tremolo 2 { a8:8 fis:8}
    \repeat tremolo 2 { a8:8 fis:8}\repeat tremolo 2 { a8:8 fis:8}
    
    \bar "."
   <ais cis,>4 
    \once \override Score.RehearsalMark.extra-offset = #'(.7 . -2)
    \rNumber #22 r4 r2   
    R1
    \tupletSpan 2
     \tuplet 3/2 { 
       r4 \ff cis,, d e fis g
       r4 fis g ais h cis
 
       r4 cis, d e fis g
       r4  e fis g ais h
       
       r4 g ais h cis d
       r4 cis d e fis g
        
     }        
     
     R1 \mark "poco ritenuto"
     R1 
     R1_"(Odette tombe das le bras du prince)"  
     R1 \mark "ritenuto."
     R1 
     \bar "||"
     \key d \major 
     \time 2/2
     \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
     \once \override Score.RehearsalMark.extra-offset = #'(0 . -2)  
     \rNumber #23
     \once \override Score.MetronomeMark.extra-offset = #'(3.5 . .5) 
     \tempo "Moderato e maestoso."
     fis1\ffff
     \tupletSpan 4
       \tuplet 3/2 {
          h,4.:8 cis:8 d:8 e:8
       }
       fis2. d4
       fis2. d4
       fis2. h,4
       
       \tuplet 3/2 {
          d4.:8 ais:8 g:8 d':8
       }
       h1(h4) 
       \tuplet 3/2 {
          cis4.:8 h:8 cis:8
       }
       fis1
       \tupletSpan 4
       \tuplet 3/2 {
          h,4.:8 cis:8 d:8 e:8
       }
       fis2. d4
       fis2. d4
       fis2. h,4
       
       \tuplet 3/2 {
          d4.:8 ais:8 g:8 d':8
       }
 


       
       h1(h2)  <h fis>2^"largamente" \ff
       \rNumber #24
       cis2 d
       e fis4 g
       a2. g4
       fis2 g4 a
       
       h2. a4 g2 a4 h4 
       cis2. h4 fis2 h,2
       cis2 d2 e2 fis4 g4
       a2. g4 fis2 g4 a4 
       h2. a4 g2 a4 h4
       \tupletSpan 1
       \tuplet 3/2 {
         c2 g c
         h1 h2
         ais2 fis ais
         h1 h2
         c2 g c
         h1 h2
         ais2 fis ais
         h1 h2
         \rNumber #25
         \bar "."
         c2 g c
         c2 g c
         g1.(g1) fis2
         c'2 g c
         c2 g c
         g1.(g1) fis2
         gis2 \ff fis gis
         gis2 fis gis
         a2 fis a 
         a2 fis a
         ais2 fis ais
         ais2 fis ais 
         ais2 e ais
         ais2 fis ais 
       }
       

          fis,4:8 gis:8 ais:8 h:8          
          cis4:8 h:8 cis:8 d:8          
          e4:8 \mark "ritenuto." d:8 e:8 fis:8      
          gis4:8 fis:8 gis:8 ais:8
       
       

        
        \bar "||"
        
      \key h \major 
       \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
       %\once \override Score.RehearsalMark.extra-offset = #'(0 . -2)  
       \rNumber #26
       \tempo "Meno Mosso."
        h2:8 \fff ais4:8 a:8
        gis2:8 fis4:8 e:8
        <h' dis,>1:8  
        <h dis,>1:8         
        <h dis,>2:8 cis,4:8 h:8
        <e ais,>2:8 <e ais,>:8 
        h8:16 h:16 cis:16 dis:16 e:16 fis:16 gis:16 ais:16 
        <h e,>1:16
        
        h2:8 \fff ais4:8 a:8
        gis2:8 fis4:8 e:8
        <h' dis,>1:8  
        <h dis,>1:8   
        <h dis,>2:8 cis,4:8 h:8
        <e ais,>2:8 <e ais,>:8 
        h8:16 h:16 cis:16 dis:16 e:16 fis:16 gis:16 ais:16 
        <h e,>2:16 h,2:16
        dis2:16 e:16 fis:16 gis4:16 ais:16
        h1:16 h2:16 h,:16 dis:16 e:16
        fis:16 gis4:16 ais:16
        
        \tuplet 3/2 {
         h2 g h
         a1 a2
         g2 e g
         fis1 fis2
         f2 d f
         e1 e2
         dis!2 h dis
         d1 d2
        }
        
        \tuplet 6/4 {
         \repeat unfold 12 {dis4}
         dis! dis h h cis cis 
         dis dis e e fis fis 
         <g h,>1.:8 <g h,>1.:8
         
         <fis ais,>1.:8
         <fis ais,>1.:8
        }
               
     
       \bar "||"
       %\once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
       \once \override Score.RehearsalMark.extra-offset = #'(0 . -2)  
       \rNumber #27      
        <fis, h,>8_"(Apparition des cygenes au dessus du lac.)"  \pp \tempo "Moderatro."   
         \repeat unfold 7 {<fis h,>8} 
         fis8 \cresc \repeat unfold 7 {fis8} 
         \repeat unfold 16 {gis8}
         \repeat unfold 16 {ais}
         \repeat unfold 16 {gis8}
         <h fis>8\fpp \repeat unfold 7 {<h fis>8}
         <h fis>8\cresc \repeat unfold 7 {<h fis>8}
         \repeat unfold 8  {dis8}
         \repeat unfold 8  {<fis cis>8}
         \repeat unfold 16 {<gis h,>8}
         <h e,>8\ff \repeat unfold 15 {<h e,>8} 
         \repeat unfold 8 {<h dis,>8}
         
         
         \rNumber #28 \bar "."
         \set countPercentRepeats = ##t
         \set repeatCountVisibility = #(every-nth-repeat-count-visible 1)
         \repeat percent 7 { \repeat unfold 8 {<h dis,>8} } \break
         h,1(h1) h1 h1 h,1\fermata
         
         
         
         
         %\once \override Score.MetronomeMark.padding =
          
%       \compressFullBarRests
%       R1_"                (Apparition des cygenes au dessus du lac.)"
%       R1*6
%       R1  
%      % \once \override Score.RehearsalMark.break-align-symbols = #'(time-signature)
%       \once \override Score.RehearsalMark.extra-offset = #'(0 . -2)  
%       R1
%       \rNumber #28
%       dis1 \ff
%       R1
%       dis1
%       R1 dis dis dis
%       R1*4
%       R1^\fermataMarkup  
   %    \bar "|."
%         
%     }
 
  %r1 mark (oh, pardonne moi, dit le prince etc. La dernère scène.)  
 
}

\layout {
    \context {
      \Score
      \override SpacingSpanner.shortest-duration-space = 2.1 %#(ly:make-moment 1/1)
    }
}
